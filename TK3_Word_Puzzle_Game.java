import java.util.Scanner;
public class TK3_Word_Puzzle_Game {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		while (true) {
			while(true){
				System.out.print("Coepoe Word Puzzle\n================\n\nRules :\n1. Create a word using given characters, min 3 characters & max 6 characters.\n2. Each level, You have 10 chances to answer correctly.\n3. To get through to next level, you have to score 70 points each level.\n\nLevel 1\n------\n\t\td\te\tt\tt\tl\ti\n");
				String arr[] = new String[10];
	            int score[] = new int[3];
	            score[0] = 0;
	            
	            for (int i = 0; i < 10; i++) {
	            	while (true) {
	            		System.out.print(i + 1 + "> Your Answer : ");
	                    arr[i] = scan.nextLine();
	                    int k = 0;
	                    
	                    for (int j = 0; j < i; j++) {
	                    	if (arr[j].equals(arr[i])) {
	                            System.out.println("You had already type this word before..");
	                            k = 1;
	                        }
	                    }
	                    if (k == 0) {
	                        break;
	                    }
	                }
	                if (arr[i].equals("die") || arr[i].equals("led") || arr[i].equals("lei") || arr[i].equals("let")
	                        || arr[i].equals("lid") || arr[i].equals("lie") || arr[i].equals("lit") || arr[i].equals("tie")
	                        || arr[i].equals("deli") || arr[i].equals("diet") || arr[i].equals("edit")
	                        || arr[i].equals("idle")
	                        || arr[i].equals("lied") || arr[i].equals("tide") || arr[i].equals("tied")
	                        || arr[i].equals("tile")
	                        || arr[i].equals("tilt") || arr[i].equals("tiled") || arr[i].equals("tilde")
	                        || arr[i].equals("title")
	                        || arr[i].equals("titled") || arr[i].equals("tilted")) {
	                    score[0] = score[0] + 10;
	                    System.out.println("#Right. Score : " + score[0]);

	                }
	            }
	            if (score[0] / 10 < 7) {
	                break;
	            }
	            System.out.println("You had answered 10 times with " + score[0] / 10 + " right answer..");
	            System.out.println();
	            System.out.println(
	                    "Correct Answers : \ndie \t led \t lei \t let \t lid\t lie\t lit\t tie\t deli\t diet\nedit \t idle \t lied\t tide\t tied\t tile\t tilt\t tilde\t tiled\t title\ntilted \t titled");
	            
	            // LEVEL 2
	            System.out.println();
				System.out.println();
	            System.out.println("Level 2\n-------\n\t\ts\te\tc\ta\te\tn\n\n");

	            for (int i = 0; i < 10; i++) {
	                while (true) {
	                    System.out.print(i + 1 + "> Your Answer : ");
	                    arr[i] = scan.nextLine();
	                    int k = 0;
	                    for (int j = 0; j < i; j++) {

	                        if (arr[j].equals(arr[i])) {
	                            System.out.println("You had already type this word before..");
	                            k = 1;
	                        }
	                    }
	                    if (k == 0) {
	                        break;
	                    }
	                }
	                if (arr[i].equals("sec") || arr[i].equals("can") || arr[i].equals("cane")
	                        || arr[i].equals("scan") || arr[i].equals("scene") || arr[i].equals("case")
	                        || arr[i].equals("cease")) {
	                    score[1] = score[1] + 10;
	                    System.out.println("#Right. Score : " + score[1]);

	                }
	            }
	            if (score[1] / 10 < 7) {
	                break;
	            }
	            System.out.println("You had answered 10 times with " + score[1] / 10 + " right answer..");
	            
	            // LEVEL 3
	            System.out.println();
				System.out.println();
	            System.out.println("Level 3\n-------\n\t\th\tk\tr\tn\te\to\n\n");

	            for (int i = 0; i < 10; i++) {
	                while (true) {
	                    System.out.print(i + 1 + "> Your Answer : ");
	                    arr[i] = scan.nextLine();
	                    int k = 0;
	                    for (int j = 0; j < i; j++) {

	                        if (arr[j].equals(arr[i])) {
	                            System.out.println("You had already type this word before..");
	                            k = 1;
	                        }
	                    }
	                    if (k == 0) {
	                        break;
	                    }
	                }
	                if (arr[i].equals("honk") || arr[i].equals("honker") || arr[i].equals("roe")
	                        || arr[i].equals("ore") || arr[i].equals("her") || arr[i].equals("hen")
	                        || arr[i].equals("one")) {
	                    score[2] = score[2] + 10;
	                    System.out.println("#Right. Score : " + score[2]);

	                }
	            }
	            if (score[2] / 10 < 7) {
	                break;
	            }
	            System.out.println("You had answered 10 times with " + score[2] / 10 + " right answer..");
	            int skor = score[0] + score[1] + score[2];
	            System.out.println("Overall score : " + skor);
	            System.out.println("You Win!!\nPress ENTER to exit..");
		            try{
		            	System.in.read();
	                	}
	                catch(Exception e){}
		            
	            }
	        
			
	            System.out.print("You Lose!! Try Again..\nDo you want to retry[y/t] ?");
	            String r = scan.nextLine();
	            if (r.equals("t")) {
	                break;
	            }
	        }
	    }

}
